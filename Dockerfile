FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get upgrade && apt-get install -y \
    build-essential \
    unzip \
    wget \
    zlib1g-dev \
    libc6 \
    && apt-get -y --purge remove libboost* \
    && apt-get remove cmake
    
RUN wget ftp://webdata2:webdata2@ussd-ftp.illumina.com/downloads/software/bcl2fastq/bcl2fastq2-v2-20-0-tar.zip \
    && unzip bcl2fastq2-v2-20-0-tar.zip \
    && tar xzvf bcl2fastq2-v2.20.0.422-Source.tar.gz --no-same-owner \
    && rm bcl2fastq2-v2-20-0-tar.zip && rm bcl2fastq2-v2.20.0.422-Source.tar.gz \
    && mkdir -p /usr/include/sys \
    && ln -s /usr/include/x86_64-linux-gnu/sys/stat.h /usr/include/sys/stat.h \
    && mkdir /tmp/bcl2fastq \
    && pwd \
    && cd /tmp/bcl2fastq \
    && /bcl2fastq/src/configure --prefix=/usr/local/ \
    && make \
    && make install \
    && cd .. && rm -r bcl2fastq \
    && cd .. && rm -r bcl2fastq 
#    && make \
#    && make install \
#    && rm -r /tmp/* \
#    && rm /usr/include/mcheck.h
RUN bcl2fastq --version

ENV VERSION 6.1.2
### Might need to update policy and keys from the 10x website. or download to NAS and store there.
RUN wget -O cellranger-$VERSION.tar.gz "https://cf.10xgenomics.com/releases/cell-vdj/cellranger-6.1.2.tar.gz?Expires=1639020686&Policy=eyJTdGF0ZW1lbnQiOlt7IlJlc291cmNlIjoiaHR0cHM6Ly9jZi4xMHhnZW5vbWljcy5jb20vcmVsZWFzZXMvY2VsbC12ZGovY2VsbHJhbmdlci02LjEuMi50YXIuZ3oiLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE2MzkwMjA2ODZ9fX1dfQ__&Signature=O9HE4lVbfg5zIsXYm1jJjPNGOJfd6wtbexZHg~gDEcbyZSbS0usvZhO--vwoaT9dUH2MuRfLYO3gcdYg3zVeJqA0O7aXWN24JV0bPFKK8jXPNnJxCL72t5cSf0z5QbmlmKAklDD~TlRQoVzV625QhdustsQFpGHZQYXzuXygHUjtfaKvveAFgA-1zChTmu0yFuNJrw~BLd9qUR8D42JeLvPag7ulmcClDF99Xj1s~hfIxpmLzT9zI3j5D6JdSPtQ1fKKMyCszj--LB2o7wyMd0Zsk2NdSLlBqkLma75IKoJM8lwZIg5yCzIk9gUZlkccdwCTJjSTMQY38-gN4-HyOA__&Key-Pair-Id=APKAI7S6A5RYOXBWRPDA" \
    && tar -xzvf cellranger-$VERSION.tar.gz 
ENV PATH /cellranger-$VERSION:$PATH

ENV GEN_VERSION 5.0.0
RUN wget -O refdata-cellranger-vdj-GRCh38-alts-ensembl-$GEN_VERSION.tar.gz "https://cf.10xgenomics.com/supp/cell-vdj/refdata-cellranger-vdj-GRCh38-alts-ensembl-$GEN_VERSION.tar.gz" \
    && tar -xzvf refdata-cellranger-vdj-GRCh38-alts-ensembl-$GEN_VERSION.tar.gz \
    && rm refdata-cellranger-vdj-GRCh38-alts-ensembl-$GEN_VERSION.tar.gz
RUN wget https://cf.10xgenomics.com/supp/cell-exp/refdata-gex-GRCh38-2020-A.tar.gz \
    && tar -xzvf refdata-gex-GRCh38-2020-A.tar.gz \
    && rm refdata-gex-GRCh38-2020-A.tar.gz

## Run cellranger test
RUN cellranger testrun --id=tiny